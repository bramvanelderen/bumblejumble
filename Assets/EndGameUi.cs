﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameUi : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
	    if (Main.Instance)
	    {
            var obj = GameObject.Find("ReturnToMainMenu");
            obj.GetComponent<Button>().onClick.AddListener(() => Main.Instance.ReturnToMenu());
        }


        var some = GameObject.Find("Quit");
        some.GetComponent<Button>().onClick.AddListener(() => Quit());
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void Quit()
    {
        print("quit works");
        Application.Quit();
    }
}
