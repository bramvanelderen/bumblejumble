﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationArrow : MonoBehaviour
{
    private Transform _target;
    [SerializeField] private LayerMask _mask;
    [SerializeField] private float _radius = 600f;
    [SerializeField] private float _detectRate = .5f;
    [SerializeField] private Vector3 _offset = new Vector3(0, 2f, 0);

    private GameObject _rotationTarget = null;

	// Use this for initialization
	void Awake () {
	    foreach (var obj in GameObject.FindObjectsOfType<NavigationArrow>())
	    {
	        if (obj != this)
	            Destroy(obj.gameObject);
	    }
	    gameObject.SetActive(false);

	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (_target)
	    {
            transform.position = _target.position + _offset;
        }

        if (_rotationTarget)
        {
            transform.LookAt(_rotationTarget.transform);
	    }
	}

    public void StartNavigating(Transform target, float duration)
    {
        _target = target;
        Destroy(gameObject, duration);
        gameObject.SetActive(true);
        StartCoroutine(DetectNearbyObjects());
    }

    IEnumerator DetectNearbyObjects()
    {
        while (gameObject.activeInHierarchy)
        {
            foreach (var item in Physics.OverlapSphere(transform.position, _radius, _mask))
            {
                if (!_rotationTarget)
                    _rotationTarget = item.gameObject;
                else
                {
                    if (Vector3.Distance(item.transform.position, _target.position) <
                        Vector3.Distance(_rotationTarget.transform.position, _target.transform.position))
                        _rotationTarget = item.gameObject;
                }
            }

            yield return new WaitForSeconds(_detectRate);
        }
    }
}
