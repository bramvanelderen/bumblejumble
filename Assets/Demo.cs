﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Demo : MonoBehaviour {

	// Use this for initialization
	void Start () {
	    if (Main.Instance == null)
	    {
	        print("Start Demo Scene");
	        StartGame();
	    }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartGame()
    {
        var gm = GameManager.Initialize(new GameObject("GameManager"), EndGame, true, false);

    }

    public void EndGame()
    {
        Destroy(GameManager.Instance.gameObject);
        ReturnToMenu();
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
