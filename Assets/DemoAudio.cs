﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoAudio : MonoBehaviour
{

    public CustomClip Clip;
    public bool Play = false;

    private AudioSource _audio;

	// Use this for initialization
	void Start ()
	{
	    _audio = gameObject.AddAudioSource();
	}
	
	// Update is called once per frame
	void Update () {
	    if (Play)
	    {
	        Clip.Play(_audio);
	        Play = false;

	    }
	}
}
