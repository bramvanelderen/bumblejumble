﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CollectableNavigation : CollectableComponent
{
    [SerializeField] private NavigationArrow _arrowPrefab;
    [SerializeField] private float _duration = 5f;
    public override void Execute(Player player)
    {
        if (_arrowPrefab)
        {
            var arrow = Instantiate(_arrowPrefab);
            arrow.StartNavigating(player.Obj.transform, _duration);
        }
    }
}

