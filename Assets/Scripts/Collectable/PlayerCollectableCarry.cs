﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollectableCarry : MonoBehaviour {

    public GameObject mCurrentCollectable {
        private set;
        get;
    }
    public Transform mAttachLocation;
    public Rigidbody mAnchorJoint;
    public GameObject mRopeObject;

    private int mCollectableSpinLimit = 15;
    private int mCollectableMass = 5;

    public void NewCollectable(Collectable collectable) {

        if(mCurrentCollectable == null) {
            GameObject nextCollectableObject = collectable.gameObject;

            nextCollectableObject.transform.position = mAttachLocation.position;

            mRopeObject.SetActive(true);

            nextCollectableObject.GetComponent<CollectableCarry>().OnHitCallback += DropCollectable;

            nextCollectableObject.AddComponent<HingeJoint>();
            nextCollectableObject.GetComponent<HingeJoint>().connectedBody = mAnchorJoint;

            nextCollectableObject.GetComponent<Rigidbody>().mass = mCollectableMass;

            HingeJoint currentJoint = nextCollectableObject.GetComponent<HingeJoint>();
            currentJoint.connectedBody = mAnchorJoint;
            currentJoint.useLimits = true;
            currentJoint.useSpring = true;

            JointLimits newlimits = currentJoint.limits;
            newlimits.max = mCollectableSpinLimit;
            newlimits.min = -mCollectableSpinLimit;
            currentJoint.limits = newlimits;

            JointSpring newSpring = currentJoint.spring;
            newSpring.spring = mAnchorJoint.GetComponent<HingeJoint>().spring.spring;
            newSpring.damper = mAnchorJoint.GetComponent<HingeJoint>().spring.damper;


            mCurrentCollectable = nextCollectableObject;
        }

    }

    void DropCollectable(GameObject obj)
    {

        mCurrentCollectable.transform.parent = null;
        mCurrentCollectable.GetComponent<CollectableCarry>().OnHitCallback -= DropCollectable;
        Destroy(mCurrentCollectable.GetComponent<HingeJoint>());
        Destroy(mCurrentCollectable.GetComponent<Rigidbody>());
        mCurrentCollectable = null;

        mRopeObject.SetActive(false);
    }

    public void DropCollectableIfCarrying() {
        if(mCurrentCollectable != null)
        {
            mCurrentCollectable.GetComponent<CollectableCarry>().Drop(null);
        }
    }
}
