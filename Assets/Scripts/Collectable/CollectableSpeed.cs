﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

public class CollectableSpeed : CollectableComponent
{
    [SerializeField] private float _speedModifier = .2f;
    [SerializeField, Tooltip("0 = infinite")] private float _duration = 0;

    public override void Execute(Player player)
    {
        player.AdjustSpeed(_speedModifier);

        StartCoroutine(SetSpeedModifier(_duration, player));
    }

    IEnumerator SetSpeedModifier(float delay, Player player)
    {
        yield return new WaitForSeconds(delay);

        player.AdjustSpeed(-_speedModifier);
    }
}