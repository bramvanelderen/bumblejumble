﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using TMPro;

public class Collectable : MonoBehaviour
{
    [SerializeField] private float _collectRadius = 3f;
    [SerializeField] private string _playerTag  = "Player";
    [SerializeField] private CustomClip _sound;
    [SerializeField] private bool _pickupByDestroy = false;
    private List<CollectableComponent> _components;
    private bool _isUsed = false;
    

    void Start()
    {
        tag = "Collectable";
        _components = new List<CollectableComponent>();
        foreach (var component in GetComponents<CollectableComponent>())
        {
            _components.Add(component);
        }
    }

    void Update()
    {
        if (_isUsed)
            return;

        foreach(var hit in Physics.OverlapSphere(transform.position, _collectRadius))
        {
            if (hit.CompareTag(_playerTag))
            {
                var player = hit.gameObject.GetComponent<Player>();
                if (player != null)
                {
                    print(gameObject.name + " is collected by: " + hit.gameObject.name);

                    Execute(player);
                }                
            }
        }
    }

    public void Refresh()
    {
        _isUsed = false;
        var light = GetComponentInChildren<Light>();
        if (light)
            light.range = 20;
    }

    public void Execute(Player player)
    {
        if (_isUsed)
            return;

        _isUsed = true;
        if (_sound.Clip != null)
        {
            var soundObj = new GameObject("SoundObject");
            Destroy(soundObj, 2f);
            _sound.Play(soundObj.AddAudioSource());
        }
        foreach (var component in _components)
        {

            component.Execute(player);
        }

        var light = GetComponentInChildren<Light>();
        if (light)
            light.range = 3;
    }

    public void Execute(GameObject obj)
    {
        var player = obj.GetComponent<Player>();
        if (player != null)
        {
            print(gameObject.name + " is collected by: " + obj.name);
            Execute(player);
        }
    }
}

