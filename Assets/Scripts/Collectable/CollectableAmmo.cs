﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CollectableAmmo : CollectableComponent
{
    [SerializeField] private int _ammoCount = 1;

    public override void Execute(Player player)
    {
        player.AddAmmo(_ammoCount);
    }
}
