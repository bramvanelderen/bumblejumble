﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class CollectableComponent : MonoBehaviour
{
    public abstract void Execute(Player player);
}

