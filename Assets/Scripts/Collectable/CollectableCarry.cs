﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableCarry : CollectableComponent
{
    public OnHit OnHitCallback;

    [SerializeField] private float _hitRadius = 2f;
    [SerializeField] private bool _destroyOnHit = false;
    [SerializeField] private List<string> _ignoreTags = new List<string>();
    private bool _isHolding = false;

    public override void Execute(Player player)
    {
        player.PickUp(this.GetComponent<Collectable>());
        _isHolding = true;
    }

    void Update()
    {
        if (!_isHolding)
            return;
        foreach (var item in Physics.OverlapSphere(transform.position, _hitRadius))
        {
            if (_ignoreTags.Contains(item.tag))
                continue;

            Drop(item.gameObject);
        }
    }

    public void Drop(GameObject obj)
    {
        if (OnHitCallback != null)
            OnHitCallback(obj);

        _isHolding = false;

        if (_destroyOnHit)
            Destroy(gameObject);
    }
}

public delegate void OnHit(GameObject obj);
