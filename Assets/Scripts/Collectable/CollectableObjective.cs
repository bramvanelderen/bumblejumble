﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CollectableObjective : CollectableComponent
{
    [SerializeField] private string _objectiveTag = "Hive";

    [SerializeField] private CustomClip _dropClip;
    private AudioSource _audio;
    private Vector3 _origin;

    public void Start()
    {
        _audio = gameObject.AddAudioSource();
        _origin = transform.position;

    }

    public override void Execute(Player player)
    {
        var comp = GetComponent<CollectableCarry>();
        if (comp)
            comp.OnHitCallback += IsHit;
    }

    void IsHit(GameObject obj)
    {
        if (obj && obj.CompareTag(_objectiveTag))
        {
            GameManager.Instance.AddCollected();
            Destroy(this.gameObject);
            print("Objective collected");
        }
        else
        {
            _dropClip.Play(_audio);
            transform.position = _origin;
            Invoke("CallRefresh", 1f);
        }
        var comp = GetComponent<CollectableCarry>();
        comp.OnHitCallback -= IsHit;
    }

    void CallRefresh()
    {
        GetComponent<Collectable>().Refresh();
    }
}