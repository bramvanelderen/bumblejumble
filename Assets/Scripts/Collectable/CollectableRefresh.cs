﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class CollectableRefresh : CollectableComponent
{
    [SerializeField] private float _delay = 5f;
    public override void Execute(Player player)
    {
        StartCoroutine(Refresh());
    }

    IEnumerator Refresh()
    {
        yield return new WaitForSeconds(_delay);
        GetComponent<Collectable>().Refresh();
    }
}

