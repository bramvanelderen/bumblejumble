﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DancingBeeAnimation : MonoBehaviour {


    [SerializeField] private Animator mAnimator;


    void Start() {
        mAnimator.SetBool("Dance", true);
    }

}
