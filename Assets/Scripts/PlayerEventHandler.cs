﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Handles external calls coming from either collectables or other stuff
/// </summary>
public class PlayerEventHandler : MonoBehaviour, Player
{
    public GameObject Obj
    {
        get { return gameObject; }
    }

    private PlayerMovement _mov;
    private WeaponSystem _wep;

    void Start()
    {
        _mov = GetComponent<PlayerMovement>();
        _wep = GetComponent<WeaponSystem>();
    }

    public void AdjustSpeed(float speed)
    {
        _mov.SpeedModifier += speed;
    }

    public void AddAmmo(int count)
    {
        _wep.AddAmmo(count);
    }

    /// <summary>
    /// Bump event happends on player collision at higher speeds
    /// </summary>
    public void Bump()
    {
        GetComponent<PlayerCollectableCarry>().DropCollectableIfCarrying();
    }

    public void Kill()
    {
        throw new NotImplementedException();
    }

    public void PickUp(Collectable collectable) {
        this.GetComponent<PlayerCollectableCarry>().NewCollectable(collectable);
    }
}