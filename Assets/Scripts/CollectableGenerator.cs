﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CollectableGenerator {

    public static Collectable GenerateCollectable()
    {
        var collectable = GenerateCollectableObject();


        return collectable;
    }

    public static Collectable GenerateObjectiveCollectable()
    {
        var collectable = GenerateCollectableObject();
        var obj = collectable.gameObject;

        obj.AddComponent<CollectableCarry>();

        return collectable;
    }

    static Collectable GenerateCollectableObject()
    {
        var obj = new GameObject();
        obj.name = "Collectable";
        return obj.AddComponent<Collectable>();
    }
}
