﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float mRotationSpeed;
    public float mJoystickDeadZone;// value less than 1

    [SerializeField] private InputAxis mRHorizontalAxis;
    [SerializeField] private InputAxis mRVerticalAxis;

    private float mHorizontalRotationAmount = 0;
    private float mVerticalRotationAmount = 0;

    void Update()
    {

        float horizontalValue = InputWrapper.GetAxis(mRHorizontalAxis);
        float verticalValue = InputWrapper.GetAxis(mRVerticalAxis);

        //Debug.Log("Hor: " + horizontalValue);
        //Debug.Log("Ver: " + verticalValue);

        if(horizontalValue > mJoystickDeadZone || horizontalValue < -mJoystickDeadZone) {
                RotateHorizontal(horizontalValue);
                 
        }
        else {
            HorizontalToZero();
        }

       if (verticalValue > mJoystickDeadZone || verticalValue < -mJoystickDeadZone) {
                RotateVertical(verticalValue);      
            
        }
        else{
            VerticalToZero();
        }

        //Debug.Log(horizontalValue);

        //Debug.Log("Current: " + mCurrentTravelVector);
        //Apply Movement
        Vector3 newVector = this.transform.rotation.eulerAngles;

        newVector.x += (Time.deltaTime * mVerticalRotationAmount);
        newVector.y += (Time.deltaTime * mHorizontalRotationAmount);

        Quaternion tempQuater = Quaternion.Euler(newVector);

        //Debug.Log("temp: " + tempVector);

        //this.transform.rotation = tempQuater;
        this.GetComponent<Rigidbody>().MoveRotation(tempQuater);

    }

    public void RotateHorizontal(float value)
    {
        //Debug.Log("Value: " + value + Input.GetAxis("RHorizontal"));
        mHorizontalRotationAmount = (mRotationSpeed * value);

    }

    public void RotateVertical(float value)
    {
        mVerticalRotationAmount = (mRotationSpeed * value * -1.0f);

    }
    
    public void HorizontalToZero()
    {
        mHorizontalRotationAmount = 0;
    }

    public void VerticalToZero()
    {
        mVerticalRotationAmount = 0;
    }

}
