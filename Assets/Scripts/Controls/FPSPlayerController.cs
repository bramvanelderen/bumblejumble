﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSPlayerController : MonoBehaviour {

    public float mMaxVelocity;
    public float mAcceleration;
    public float mJoystickDeadZone;

    [SerializeField] private InputAxis mHorizontalAxis;
    [SerializeField] private InputAxis mVerticalAxis;

    private Vector3 mCurrentTravelVector;

    //private float mScaledMaxVelocity;
    //private float mScaledAcceleration;


    void Update(){

        float horizontalValue = InputWrapper.GetAxis(mHorizontalAxis);
        float verticalValue = InputWrapper.GetAxis(mVerticalAxis);

        mCurrentTravelVector = Vector3.zero;

        if (horizontalValue > mJoystickDeadZone || horizontalValue < mJoystickDeadZone) {
            
            MoveHorizontal(horizontalValue);
        }else {
            HorizontalToZero();
        }

        if (verticalValue > mJoystickDeadZone || verticalValue < mJoystickDeadZone) {
            MoveForwardBack(verticalValue);
        }else {
            VerticalToZero();
        }

        if (Input.GetButton("Rb"))
        {

        }

        Vector3 tempVector = Time.deltaTime * mCurrentTravelVector;        

        this.GetComponent<Rigidbody>().MovePosition(this.transform.position + tempVector);
    }

    public void MoveHorizontal(float value){
        /*Vector3 changeVector*/
        mCurrentTravelVector += this.transform.right * (mAcceleration * value );
        //Debug.Log(this.transform.right + " + " + mAcceleration + " * " + value + " = " + mCurrentTravelVector);
        //ChangeTheTravelVector(changeVector);  
    }


    public void MoveForwardBack(float value){
        /*Vector3 changeVector*/
        mCurrentTravelVector += this.transform.forward * (mAcceleration * value);

        //ChangeTheTravelVector(changeVector);
    }

    public void HorizontalToZero(){
       
        mCurrentTravelVector.x = 0;
    }

    public void VerticalToZero(){
        mCurrentTravelVector.z = 0;
    }

    public void MoveUp()
    {

    }

    public void MoveDown()
    {

    }



}
