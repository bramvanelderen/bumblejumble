﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    [SerializeField] private CustomClip _collisionSound;
    [SerializeField] private float velocityThresholdBump;
    [SerializeField] private float _knockbackVelocity = 20f;
    [SerializeField] private float _disablePlayerControlDuration = .2f;
    [SerializeField] private float _distanceInterval = 3f;
    [SerializeField] private float _checkDistanceInterval = .1f;

    private Rigidbody _rb;
    private AudioSource _audio;
    private PlayerMovement _pm;
    private Player _player;
    private float _distanceCounter = 0f;
    private Vector3 _lastLocation;

    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _pm = GetComponent<PlayerMovement>();
        _player = GetComponent<Player>();
        _audio = gameObject.AddAudioSource();
        _lastLocation = transform.position;

        StartCoroutine(CheckDistance());
    }

    void Update()
    {
        
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Player") && !collision.gameObject.CompareTag("Collectable") && _distanceCounter == 0)
        {
            if (collision.relativeVelocity.magnitude > velocityThresholdBump)
            {
                _collisionSound.Play(_audio);
                _distanceCounter = _distanceInterval;
                StartCoroutine(DisablePlayerControl());
                _rb.angularVelocity = Vector3.zero;
                var point = collision.contacts[0].point;
                _rb.velocity = (transform.position - (point)).normalized*_knockbackVelocity;
                _player.Bump();
            }
        }
    }

    IEnumerator CheckDistance()
    {
        while (true)
        {
            var dist = Vector3.Distance(_lastLocation, transform.position);
            if (dist > .5f)
                _distanceCounter -= dist;
            if (_distanceCounter < 0)
                _distanceCounter = 0;
            _lastLocation = transform.position;
            yield return new WaitForSeconds(_checkDistanceInterval);
        }
    }

    IEnumerator DisablePlayerControl()
    {
        _pm.PlayerControl = false;

        yield return new WaitForSeconds(_disablePlayerControlDuration);

        _pm.PlayerControl = true;
    }
}