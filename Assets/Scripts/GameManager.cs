﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// 
/// </summary>
public class GameManager : Singleton<GameManager>
{
    enum GameState
    {
        Initializing,
        InGame,
        Finished,
    }

    private Vector3 _playerSpawn;
    private GameObject _collectableSpawnsContainer;
    private GameObject _otherSpawnsContainer;

    private GameManagerData _data;
    private GameState _state;
    private float _totalTime = 0;
    private int _totalCollectablesSpawned;
    private int _totalCollectablesRequired;
    private int _collectedCount = 0;
    private GameObject _player;

    private Action _endGame;

    private bool _initialized = false;
    private bool _waitOnLoad = false;
    private bool _challengeMode = false;

    private AudioSource _audio;

    /// <summary>
    /// Should be called on scene loaded
    /// </summary>
    void StartGame()
    {
        if (_state != GameState.Initializing)
            return;
        if (_playerSpawn == Vector3.zero)
        {
            var obj = GameObject.Find("PlayerSpawn");
            if (!obj)
                return;

            _playerSpawn = obj.transform.position;
        }

        if (_collectableSpawnsContainer == null)
        {
            var obj = GameObject.Find("CollectableSpawns");
            if (obj == null)
                return;

            _collectableSpawnsContainer = obj;
        }

        if (_otherSpawnsContainer == null)
        {
            var obj = GameObject.Find("OtherSpawns");
            if (obj == null)
                return;

            _otherSpawnsContainer = obj;
        }

        SpawnNewPlayer();
        SpawnCollectables();
        _audio = gameObject.AddAudioSource();
        _state = GameState.InGame;
    }

	// Update is called once per frame
	void Update () {

	    switch (_state)
	    {
            case GameState.Initializing:
	            if (!_initialized)
	                InitializeLocal(null);
                if (!_waitOnLoad)
	                StartGame();
	            break;
            case GameState.InGame:
	            _totalTime += Time.deltaTime;

	            if (!_challengeMode)
	                UIEventHandler.OnTimeChange(_totalTime);
	            else
	                UIEventHandler.OnTimeChange(_data.ChallengeModeTimeLimit - _totalTime);

                if (_collectedCount == _totalCollectablesRequired)
	                FinishGame();
	            if (_challengeMode & _totalTime > _data.ChallengeModeTimeLimit)
	                FinishGame();
	            break;
            case GameState.Finished:

	            break;
	    }
	}

    void FinishGame()
    {
        _state = GameState.Finished;

        if (PlayerPrefs.HasKey("BestTime"))
        {
            if (PlayerPrefs.GetFloat("BestTime") > _totalTime)
            {
                //NEW BEST TIME YAY
                PlayerPrefs.SetFloat("BestTime", _totalTime);
            }
        }
        else
        {
            PlayerPrefs.SetFloat("BestTime", _totalTime);
        }

        if (_endGame != null)
            _endGame();
    }

    public void AddCollected()
    {
        _collectedCount++;
        UIEventHandler.OnScore(_collectedCount);
        _data.CollectSound.Play(_audio);
    }

    GameObject SpawnNewPlayer()
    {
        if (_player)
            DestroyPlayer(_player);

        foreach (var item in GameObject.FindGameObjectsWithTag("Player"))
        {
            DestroyPlayer(item);
        }

        _player = Instantiate(_data.PlayerPrefab, _playerSpawn, Quaternion.identity);

        return _player;
    }

    void SpawnCollectables()
    {
        var locations = new List<Vector3>();
        for (int i = 0; i < _collectableSpawnsContainer.transform.childCount; i++)
        {
            locations.Add(_collectableSpawnsContainer.transform.GetChild(i).position);
        }

        _totalCollectablesSpawned = 0;
        _totalCollectablesRequired = _data.TotalCollectablesRequired;
        for (int i = 0; i < _data.TotalCollectablesSpawned; i++)
        {
            if (locations.Count == 0)
                continue;
            var location = locations[UnityEngine.Random.Range(0, locations.Count)];
            locations.Remove(location);
            Instantiate(_data.CollectableObjectivePrefab, location, Quaternion.identity);
            if (_totalCollectablesSpawned != _data.TotalCollectablesSpawned)
                _totalCollectablesSpawned++;
        }

        locations = new List<Vector3>();
        for (int i = 0; i < _otherSpawnsContainer.transform.childCount; i++)
        {
            locations.Add(_otherSpawnsContainer.transform.GetChild(i).position);
        }

        for (int i = 0; i < _data.TotalOthers; i++)
        {
            if (locations.Count == 0)
                continue;
            var location = locations[UnityEngine.Random.Range(0, locations.Count)];
            locations.Remove(location);
            Instantiate(_data.OtherCollectablePrefabs[UnityEngine.Random.Range(0, _data.OtherCollectablePrefabs.Count)], location, Quaternion.identity);
        }
    }

    void DestroyPlayer(GameObject player)
    {
        Destroy(player);
    }

    void InitializeLocal(Action endGameCallback)
    {
        _endGame = endGameCallback;
        _state = GameState.Initializing;
        _data = Resources.Load<GameManagerData>("GameData");
        _totalTime = 0;
        _collectedCount = 0;
        _initialized = true;
    }

    public static GameManager Initialize(GameObject obj, Action endGameCallback, bool challengeMode, bool waitOnLoad)
    {
        if (GameManager.Instance == null)
            obj.AddComponent<GameManager>();
        GameManager.Instance.InitializeLocal(endGameCallback);
        GameManager.Instance._waitOnLoad = waitOnLoad;
        GameManager.Instance._challengeMode = challengeMode;
        if (waitOnLoad)
            SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        return GameManager.Instance;
    }

    private static void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        GameManager.Instance.StartGame();
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
    }
}
