﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSystem : MonoBehaviour {

    public int mMaxAmmo;
    public int mStartingAmmo;
    public float mFireTimeCD = 2.0f;
    public Transform mFirePoint;

    public GameObject mProjectile;
    private int mCurrentAmmo;

    private float mFireWaitTime = 0.0f;
    private float _shootDelay = 0.175f;

    private PlayerAnimation _pa;
    private bool _isShooting = false;
    [SerializeField] private CustomClip _shootClip;
    private AudioSource _audio;

    void Start()
    {
        _pa = GetComponent<PlayerAnimation>();
        mCurrentAmmo = mStartingAmmo;
        _audio = gameObject.AddAudioSource();
    }

    void Update(){
        if (InputWrapper.GetButton(InputButtons.A) && !_isShooting) {
           
            if (mFireTimeCD < mFireWaitTime ) {
                mFireWaitTime = 0.0f;
                _pa.Shoot();
                _isShooting = true;
                Invoke("FireProjectile", _shootDelay);
               
            }
            
        }

        mFireWaitTime += Time.deltaTime;
    }

    public void AddAmmo(int ammoToAdd){
        if(mCurrentAmmo < mMaxAmmo) {
            mCurrentAmmo += ammoToAdd;

            if(mCurrentAmmo > mMaxAmmo) {
                mCurrentAmmo = mMaxAmmo;
            }
        }
    }

    private void FireProjectile()
    {
        var projectile = Instantiate(mProjectile);
        projectile.transform.position = mFirePoint.position;
        projectile.transform.forward = mFirePoint.forward;
        projectile.GetComponent<Projectile>().SetOrigin(gameObject);
        _isShooting = false;
        _shootClip.Play(_audio);
        _pa.Idle();
    }
}
