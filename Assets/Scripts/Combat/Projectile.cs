﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    [SerializeField] private float _speed = 100f;
    [SerializeField] private float _lifetime = 5f;
    [SerializeField] private float _radius = 1f;

    public int mDamage;
    public int mProjectileEffectLayer;

    private GameObject mOrigin;

    void Start()
    {
        Destroy(gameObject, _lifetime);
    }

    void Update()
    {
        foreach (var item in Physics.SphereCastAll(transform.position, _radius, transform.forward, _speed*Time.deltaTime))
        {
            if (item.transform == transform)
                continue;
            if (item.transform.CompareTag("Player"))
                continue;

            var hp = item.transform.gameObject.GetComponent<Health>();
            if (hp)
                hp.DealDamage(mDamage);

            var collect = item.transform.gameObject.GetComponent<Collectable>();

            if (collect)
                collect.Execute(mOrigin);

            Destroy(this.gameObject);
        }

        transform.position += transform.forward*_speed*Time.deltaTime;
    }


    public void SetOrigin(GameObject obj)
    {
        mOrigin = obj;
    }
}
