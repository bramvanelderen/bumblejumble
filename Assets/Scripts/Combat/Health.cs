﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public int mStartingHealth;

    public int mCurrentHealth {
        private set;
        get;
    }

    void Start() {
        mCurrentHealth = mStartingHealth;
    }

    public void DealDamage(int damage) {
        mCurrentHealth -= damage;

        if(mCurrentHealth <= 0) {
            Destroy(this.gameObject);
        }
    }
}
