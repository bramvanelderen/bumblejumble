﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour {

    public int mStoredQuantity;
    public string mPlayerTag = "Player";

    public void OnTriggerEnter(Collider otherCollider) {
        
        if(otherCollider.tag == mPlayerTag) {
            otherCollider.GetComponent<WeaponSystem>().AddAmmo(mStoredQuantity);
            Destroy(gameObject);
        }
    }
}
