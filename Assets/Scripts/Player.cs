﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface Player
{
    GameObject Obj { get; }
    void AdjustSpeed(float speed);
    void AddAmmo(int count);
    void Kill();
    void PickUp(Collectable collectable);
    void Bump();
}