﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
    public float SpeedModifier { get; set; }
    [SerializeField]
    private float _deaccelerateRate = 2f;
    [SerializeField]
    private float _angularDeaccel = 2f;
    [SerializeField]
    private AnimationCurve _accel;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _elevationSpeed = 10f;
    [SerializeField]
    private float _rotationSpeed;
    [SerializeField]
    private InputAxis Horizontal;
    [SerializeField]
    private InputAxis Vertical;

    private Transform _camera;
    private Rigidbody _rb;
    private Vector3 _input;
    private Vector3 _elevation = Vector3.zero;

    private float _accelModifier = 0f;
    public bool PlayerControl = true;



    // Use this for initialization
    void Start()
    {
        _rb = GetComponent<Rigidbody>();

        var camera = CameraFollow.Instance;
        camera.Target = transform;
        _camera = camera.transform;

        SpeedModifier = 1f;
    }

    void Update()
    {
        _elevation = new Vector3(0, InputWrapper.GetAxis(InputAxis.Rt, true) - InputWrapper.GetAxis(InputAxis.Lt, true), 0);
        _input = new Vector3(InputWrapper.GetAxis(Horizontal), 0, InputWrapper.GetAxis(Vertical));
        if (_input.x != 0 || _input.z != 0)
        {
            var angle = -1 * (Mathf.Atan2(_input.z, _input.x) * Mathf.Rad2Deg - 90);

            if (PlayerControl)
                transform.rotation = Quaternion.RotateTowards(transform.rotation,
                    Quaternion.Euler(0, _camera.eulerAngles.y, 0f),
                    _rotationSpeed * Time.deltaTime);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (_input.x != 0 || _input.z != 0 || _elevation.y != 0)
        {
            _accelModifier += Time.deltaTime * 1f;
            if (_accelModifier > 1f)
                _accelModifier = 1f;
        }
        else
        {
            _accelModifier -= Time.deltaTime * 2f;
            if (_accelModifier < 0)
                _accelModifier = 0;
        }
        var vel = _rb.velocity;
        if (_input.x != 0 || _input.z != 0 || _elevation.y != 0)
        {
            var direction = Vector3.ClampMagnitude(_input, 1f);
            vel = transform.rotation * direction * _speed * SpeedModifier * _accel.Evaluate(_accelModifier);
            vel += (_elevation * _elevationSpeed * _accel.Evaluate(_accelModifier));
        }
        else
        {
            vel = Vector3.MoveTowards(_rb.velocity, Vector3.zero, _deaccelerateRate * Time.deltaTime);
        }

        if (PlayerControl)
        {
            _rb.velocity = vel;
            _rb.angularVelocity = Vector3.MoveTowards(_rb.angularVelocity, Vector3.zero, _angularDeaccel * Time.deltaTime);
        }
    }
}
