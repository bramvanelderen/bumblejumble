﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCircle : MonoBehaviour {

    public Transform mCamera;
    public float mMoveSpeed;//per Second

    private void Update() {
        mCamera.RotateAround(this.transform.position, -mCamera.up, mMoveSpeed * Time.deltaTime);
    }

}
