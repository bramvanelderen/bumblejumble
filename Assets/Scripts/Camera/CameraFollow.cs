﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Orbits around the selected target based on values in CameraData
/// </summary>
public class CameraFollow : Singleton<CameraFollow>
{
    public Transform Target { private get; set; }

    private CameraData _data;
    private float _lastInput = 0;
    private Vector3 _currentEuler = Vector3.zero;
    private Vector3 _position = Vector3.zero;

	// Use this for initialization
	void Start ()
	{
	    _data = Resources.Load<CameraData>("CameraData");
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (!Target)
            return;

        var h = InputWrapper.GetAxis(_data.Horizontal.Axis) * (_data.Horizontal.inverted ? -1 : 1);
        var v = InputWrapper.GetAxis(_data.Vertical.Axis) * (_data.Vertical.inverted ? -1 : 1);
        if (h != 0 || v != 0)
        {
            _currentEuler.x += _data.RotationSpeedInput.x * v;
            _currentEuler.y += _data.RotationSpeedInput.y * h;
            

            transform.eulerAngles = _currentEuler;
            _lastInput = Time.time;
        }

        if (Time.time - _lastInput > _data.DisableRotationAfterInput)
	    {
	        transform.rotation = Quaternion.RotateTowards(transform.rotation, Target.rotation * Quaternion.Euler(_data.OffsetRotation),
                _data.RotationSpeed.x*Time.deltaTime);
	        _currentEuler = transform.eulerAngles;
        }

	    _position = Target.position;
        Vector3 position = _position + (transform.rotation * _data.OffsetPosition);
        var ray = new Ray(Target.position, (position - Target.position).normalized);
        foreach (var hit in Physics.RaycastAll(ray, Vector3.Distance(Target.position, position)))
	    {
	        if (hit.transform != Target && hit.transform != transform && !_data.IgnoreTags.Contains(hit.transform.tag))
	        {
	            if (Vector3.Distance(Target.position, hit.point) < Vector3.Distance(Target.position, position))
	            {
	                position = hit.point;
	            }
	        }
	    }
	    transform.position = Vector3.MoveTowards(transform.position, position, _data.MoveSpeed*Time.deltaTime);
	}
}
