﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTest : MonoBehaviour
{

    [SerializeField] private InputAxis _axis;

	// Use this for initialization
	void Start ()
	{
	    CameraFollow.Instance.Target = transform;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    print(InputWrapper.GetAxis(_axis));
	}
}
