﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEventHandler {

    public delegate void Score(int newScore);
    public static event Score Scored;
    public static void OnScore(int newScore) {
        if(Scored != null) {
            Scored.Invoke(newScore);
        }
    }

    public delegate void TimeChange(float timeInSeconds);
    public static event TimeChange TimeChanged;
    public static void OnTimeChange(float timeInSeconds) {
        if(TimeChanged != null) {
            TimeChanged(timeInSeconds);
        }
    }

}
