﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenuControllerControls : MonoBehaviour {

    public List<Button> mSelections;
    [Range(0,1)]
    public float mJoystickDeadZone;
    public Color mSelectionColor;

    private int mCurrentSelection = 0;
    private Color mNormalColor;
    private float mTimeBetweenSelectionChanges = 0.30f;
    private float mCurrentTime = 1.0f;

    private void Start() {

        TextMeshProUGUI tempTMPro = mSelections[mCurrentSelection].GetComponentInChildren<TextMeshProUGUI>();
        mNormalColor = tempTMPro.colorGradient.topLeft;
   
        HighlightCurrentSelection();
    }

    private void Update() {

        mCurrentTime += Time.deltaTime;

        if(mCurrentTime >= mTimeBetweenSelectionChanges) {
            
            if (InputWrapper.GetAxis(InputAxis.LVertical) < -mJoystickDeadZone) {
                mCurrentTime = 0.0f;
                if (mCurrentSelection < mSelections.Count - 1) {
                    mCurrentSelection++;
                }
                else {
                    mCurrentSelection = 0;
                }

            }

            if (InputWrapper.GetAxis(InputAxis.LVertical) > mJoystickDeadZone) {
                mCurrentTime = 0.0f;
                if (mCurrentSelection > 0) {
                    mCurrentSelection--;
                }
                else {
                    mCurrentSelection = mSelections.Count - 1;
                }

            }

            HighlightCurrentSelection();

            if (InputWrapper.GetButtonDown(InputButtons.A) || InputWrapper.GetButtonDown(InputButtons.X)) {
                mSelections[mCurrentSelection].onClick.Invoke();
            }
        }
       
    }

    private void HighlightCurrentSelection() {

        TextMeshProUGUI temp;

        for (int i = 0; i < mSelections.Count; i++) {
            temp = mSelections[i].GetComponentInChildren<TextMeshProUGUI>();
            temp.color = mNormalColor;
            temp.enableVertexGradient = true;
        }

        temp = mSelections[mCurrentSelection].GetComponentInChildren<TextMeshProUGUI>();
        temp.color = mSelectionColor;
        temp.enableVertexGradient = false;

    }

}
