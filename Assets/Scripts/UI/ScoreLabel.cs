﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreLabel : MonoBehaviour {

    public Text mLabel;

    private void Start() {
        UIEventHandler.Scored += UpdateLabel;

    }

    void UpdateLabel(int newScore) {
        mLabel.text = newScore.ToString();
    }
}
