﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreUIOnHive : MonoBehaviour {

    public List<GameObject> mScoreUI;//These should be int heorder they are destroyed
    public List<TextMeshPro> mTimerText;//First is minutes, Second is seconds

	// Use this for initialization
	void Start () {
        UIEventHandler.Scored += ScoredCollectable;
        UIEventHandler.TimeChanged += UpdateTimer;
    }

    private void OnDestroy() {
        UIEventHandler.Scored -= ScoredCollectable;
        UIEventHandler.TimeChanged -= UpdateTimer;
    }

    private void ScoredCollectable(int newScore) {

        if(mScoreUI.Count > 0) {
            GameObject tempGameObject = mScoreUI[mScoreUI.Count - 1];
            mScoreUI.Remove(tempGameObject);
            Destroy(tempGameObject);
            
        }  
    }

    private void UpdateTimer(float timeInSeconds) {

        int minutes = (int)(timeInSeconds / 60);
        int seconds = (int)(timeInSeconds) % 60;

        Debug.Assert(mTimerText.Count == 2);

        if(minutes < 10) {
            mTimerText[0].text = "0" + minutes;
        }
        else {
            mTimerText[0].text = minutes.ToString();
        }

        if (seconds < 10) {
            mTimerText[1].text = "0" + seconds;
        }
        else {
            mTimerText[1].text = seconds.ToString();
        }

    }
}
