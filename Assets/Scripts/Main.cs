﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Main : Singleton<Main>
{

    [SerializeField] private CustomClip explorermusic;
    [SerializeField] private CustomClip challengemusic;
    public StartOptions mStartOptions;

    private bool _challenge = false;

    private GameObject _musicobj;
    private AudioSource _aud;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

	// Use this for initialization
	void Start ()
	{
	    _musicobj = new GameObject("music");
	    DontDestroyOnLoad(_musicobj);
	    _aud = _musicobj.AddAudioSource();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartGame()
    {
        _challenge = false;
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        SceneManager.LoadScene(1);
    }

    public void StartGameChallengeMode()
    {
        _challenge = true;
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        SceneManager.LoadScene(1);
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        Cursor.lockState = CursorLockMode.Locked;
        var gm = GameManager.Initialize(new GameObject("GameManager"), EndGame, _challenge, false);
        if (_challenge)
        {
            challengemusic.Play(_aud);
        }
        else
        {
            explorermusic.Play(_aud);
        }
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
    }

    public void EndGame()
    {
        Destroy(GameManager.Instance.gameObject);
        mStartOptions.animColorFade.SetTrigger("fade");
        SceneManager.LoadScene(2);
    }

    public void TestFade()
    {
        mStartOptions.animColorFade.SetTrigger("fade");
        SceneManager.LoadScene(2);
    }

    public void ReturnToMenu()
    {
        Cursor.lockState = CursorLockMode.None;
        Destroy(_musicobj);
        Destroy(GameObject.Find("UI"));
        Destroy(gameObject);
        SceneManager.LoadScene(0);
    }
}
