﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "Data/GameData")]
public class GameManagerData : ScriptableObject
{
    public int TotalCollectablesRequired = 4;
    public int TotalCollectablesSpawned = 7;
    public int TotalOthers = 16;
    public GameObject PlayerPrefab;
    public GameObject CollectableObjectivePrefab;
    public List<GameObject> OtherCollectablePrefabs;
    public CustomClip CollectSound;
    public CustomClip ExplorerMusic;
    public CustomClip ChallengeMusic;
    public float ChallengeModeTimeLimit = 120;
}
