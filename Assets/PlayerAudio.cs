﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{

    [SerializeField] private CustomClip _idle;

	// Use this for initialization
	void Start ()
	{
	    _idle.Play(gameObject.AddAudioSource());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
