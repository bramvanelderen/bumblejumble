﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RetrieveHighScores : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI _label;
    [SerializeField] private TextMeshProUGUI _value;

    // Use this for initialization
    void Start () {
        if (!PlayerPrefs.HasKey("BestTime"))
        {
            _label.text = "";
            _value.text = "";
        }
        else
        {
            _label.text = "BEST TIME:";
            var score = PlayerPrefs.GetFloat("BestTime");
            int minutes = (int)(score / 60);
            int seconds = (int)(score) % 60;
            var labelText = "";
            labelText += (minutes > 10) ? minutes.ToString() : "0" + minutes;
            labelText += ":";
            labelText += (seconds > 10) ? seconds.ToString() : "0" + seconds;
            _value.text = labelText;

        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
