﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagAnimator : MonoBehaviour
{
    public float speed = 10f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    transform.eulerAngles += new Vector3(0, speed*Time.deltaTime, 0);
	}
}
