﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    [SerializeField] private Vector3 _maxRotations = new Vector3(15, 0, 10);
    [SerializeField] private Transform _modelRoot;
    [SerializeField] private float _shootAnimationDuration;
    private Vector3 _additiveVector = Vector3.zero;

    [SerializeField] private Animator _anim;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
	    if (_modelRoot)
	    {
	        var h = InputWrapper.GetAxis(InputAxis.LHorizontal);
            var v = InputWrapper.GetAxis(InputAxis.LVertical);
	        var input = Vector3.ClampMagnitude(new Vector3(h, 0, v), 1f);

	        var rotation = transform.eulerAngles;
	        rotation.x = Mathf.Lerp(0, _maxRotations.x, Mathf.Abs(v));
            rotation.z = Mathf.Lerp(0, _maxRotations.z, Mathf.Abs(h));
	        rotation.x *= v < 0 ? -1 : 1;
            rotation.z *= h < 0 ? 1 : -1;

	        _modelRoot.eulerAngles = rotation;
            _additiveVector = new Vector3(0f, Mathf.Sin(Time.time * 8), 0f);
	        transform.position += _additiveVector / 80;
	    }
	}

    public void Shoot()
    {
        _anim.SetBool("Idle", false);
        _anim.SetBool("Shoot", true);
    }

    public void Idle()
    {
        _anim.SetBool("Shoot", false);
        _anim.SetBool("Idle", true);
    }
}
